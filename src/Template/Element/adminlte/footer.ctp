<!-- To the right -->
<div class="pull-right hidden-xs">
  Sistema de Estoque do Curso
</div>
<!-- Default to the left -->
<strong>Copyright &copy; <?=date('Y');?> <a href="https://www.schoolofnet.com.br">School of Net</a>.</strong> Todos os direitos reservados!
